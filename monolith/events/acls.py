import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_locationPic(city, state):

    queryURL = "https://api.pexels.com/v1/search"
    location = {"query": f"{city}, {state}"}
    key = {"Authorization": PEXELS_API_KEY}

    response = requests.get(queryURL, params=location, headers=key)
    response_json = json.loads(response.content)

    response_dict = {
        "picURL": response_json["photos"][0]["url"]
    }

    return response_dict["picURL"]


def get_weather(city, state):

    queryURL_coords = "http://api.openweathermap.org/geo/1.0/direct"
    specs_coords = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
    }

    response_coords = requests.get(queryURL_coords, params=specs_coords)
    response_coords_json = json.loads(response_coords.content)

    queryURL_weather = "https://api.openweathermap.org/data/2.5/weather"

    specs_weather = {
        "lat": response_coords_json[0]["lat"],
        "lon": response_coords_json[0]["lon"],
        "appid": OPEN_WEATHER_API_KEY,
    }

    response_weather = requests.get(queryURL_weather, params=specs_weather)
    response_weather_json = json.loads(response_weather.content)

    response_dict = {
        "temp": response_weather_json["main"]["temp"],
        "desc": response_weather_json["weather"][0]["description"],
    }

    return response_dict
